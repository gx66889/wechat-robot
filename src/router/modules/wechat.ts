import type { RouteRecordRaw } from 'vue-router'

function Layout() {
  return import('@/layouts/index.vue')
}

const routes: RouteRecordRaw = {
  path: '/wechat',
  component: Layout,
  redirect: '/wechat',
  name: 'wechat',
  meta: {
    title: '微信配置',
    icon: 'i-heroicons-solid:menu-alt-3',
  },
  children: [
    {
      path: 'white',
      name: 'white',
      component: () => import('@/views/wechat/white.vue'),
      meta: {
        title: '群白名单',
      },
    },
    {
      path: 'chatRecord',
      name: 'chatRecord',
      component: () => import('@/views/wechat/chatRecord.vue'),
      meta: {
        title: '聊天记录表',
      },
    },
    {
      path: 'chatHelp',
      name: 'chatHelp',
      component: () => import('@/views/wechat/chatHelp.vue'),
      meta: {
        title: '帮助助手',
      },
    },
    {
      path: 'chatApater',
      name: 'chatApater',
      component: () => import('@/views/wechat/chatApater.vue'),
      meta: {
        title: '适配器记录',
      },
    },
    {
      path: 'chatApi',
      name: 'chatApi',
      component: () => import('@/views/wechat/chatApi.vue'),
      meta: {
        title: '对外接口',
      },
    },
    {
      path: 'paramMap',
      name: 'paramMap',
      component: () => import('@/views/wechat/paramMap.vue'),
      meta: {
        title: '参数映射表',
      },
    },
  ],
}

export default routes
